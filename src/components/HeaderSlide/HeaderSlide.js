import React from "react";
import "./HeaderSlide.scss";
import close_icon from "./images/close.png";
import home_icon from "./images/home.png";
import favotite_icon from "./images/star.png";

class HeaderSlide extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className={this.props.state ? "header-slide--open" : "header-slide"}>
        <div className="header-slide__content">
          <div
            className="header-slide__header"
            onClick={this.props.clickButton}
          >
            <img src={close_icon} alt="" className="header-slide__icon-close" />
          </div>
          <div className="header-slide__body">
            <div className="header-slide__items">
              <div className="header-slide__home">
                <a href="/" className="header-slide__i">
                  <div>
                    <img src={home_icon} alt="" />
                  </div>
                  <div>
                    <span>Home</span>
                  </div>
                </a>
              </div>
              <div className="header-slide__favorite">
                <a className="header-slide__i" href="/">
                  <div>
                    <img src={favotite_icon} alt="" />
                  </div>
                  <div>
                    <span>Favorite</span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default HeaderSlide;

import React from "react";
import "./ContentBox.scss";

class ContentBox extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="content-box">
        {this.props.massOfBeers.map((element, index, mass) => {
          return (
            <div className="content-box__main" key={index}>
              <div className="content-box__element">
                <div className="content-box__header">
                  <div className="content-box__image" />
                </div>
                <div className="content-box__content">
                  <div className="content-box__title">Beer Jateskiy gus</div>
                  <div className="content-box__description">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                    Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                  </div>
                  <div className="content-box__button">
                    <a href="/page" className="content-box__open">
                      Open
                    </a>
                    <div className="content-box__favorite">Add to favorite</div>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

export default ContentBox;

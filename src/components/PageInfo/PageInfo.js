import React from "react";
import MainHeader from "../MainHeader/MainHeader";
import "./PageInfo.scss";
import InfoLogo from "./images/information.png";

class PageInfo extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div>
        <header>
          <MainHeader />
        </header>
        <main className="page-info__info">
          <section>
            <div className="page-info__header-section">
              <div className="page-info__header--left">
                <div className="page-info__title">PUNC IPA 2007-2015</div>
                <div className="page-info__mini-description">
                  Post Modern Classic. Spiky. Tropical. Hoppy.
                </div>
                <div className="page-info__button">ADD TO FAVORITES</div>
                <div className="page-info__description">
                  Cool Fancy Text Generator is a copy and paste font generator
                  and font changer that creates Twitter, Facebook, Instagram
                  fonts. It converts a normal text to different free cool fonts
                  styles, such as tattoo fonts, calligraphy fonts, web script
                  fonts, cursive fonts, handwriting fonts, old English fonts,
                  word fonts, pretty fonts, font art... Facebook, Twitter,
                  Instagram Fonts or Fonts for Instagram, Twitter, Facebook - If
                  that is what you want then this tool is a perfect place to go
                  because it provides more than that!Cool Fancy Text Generator
                  is a copy and paste font generator and font changer that
                  creates Twitter, Facebook, Instagram fonts. It converts a
                  normal text to different free cool fonts styles, such as
                  tattoo fonts, calligraphy fonts, web script fonts, cursive
                  fonts, handwriting fonts, old English fonts, word fonts,
                  pretty fonts, font art... Facebook, Twitter, Instagram Fonts
                  or Fonts for Instagram, Twitter, Facebook - If that is what
                  you want then this tool is a perfect place to go because it
                  provides more than that!
                </div>
              </div>
              <div className="page-info__header--right" />
            </div>
          </section>
          <section className="page-info__stat--left">
            <div className="page-info__main-info">
              <div className="page-info__interval1">
                <div className="page-info__stat-title">Properties</div>
                <div className="page-info__values">
                  <div className="page-info__item">
                    <div className="page-info__item--title">ABV</div>
                    <div className="page-info__item--info">
                      <img src={InfoLogo} alt="" />
                    </div>
                    <div className="page-info__item--value">1000</div>
                  </div>
                  <div className="page-info__item">
                    <div className="page-info__item--title">IBU</div>
                    <div className="page-info__item--info">
                      <img src={InfoLogo} alt="" />
                    </div>
                    <div className="page-info__item--value">1000</div>
                  </div>
                  <div className="page-info__item">
                    <div className="page-info__item--title">EBC</div>
                    <div className="page-info__item--info">
                      <img src={InfoLogo} alt="" />
                    </div>
                    <div className="page-info__item--value">1000</div>
                  </div>
                </div>
              </div>
              <div className="page-info__interval2">
                <div className="page-info__stat-title">Food Pairing</div>
                <div className="page-info__values">
                  <div className="page-info__item">
                    Aenean vulputate eleifend tellus. Aenean leo ligula
                  </div>
                  <div className="page-info__item">
                    Aliquam lorem ante, dapibus in, viverra quis, feugiat a,
                    tellus.
                  </div>
                  <div className="page-info__item">
                    Maecenas tempus, tellus eget condimentu.
                  </div>
                </div>
              </div>
            </div>
            <div className="page-info__additional-info">
              <div className="page-info__description">
                <div className="page-info__stat-title">Brewing</div>
                <div className="page-info__description">
                  Cool Fancy Text Generator is a copy and paste font generator
                  and font changer that creates Twitter, Facebook, Instagram
                  fonts. It converts a normal text to different free cool fonts
                  styles, such as tattoo fonts, calligraphy fonts, web script
                  fonts, cursive fonts, handwriting fonts, old English fonts,
                  word fonts, pretty fonts, font art... Facebook, Twitter,
                  Instagram Fonts or Fonts for Instagram, Twitter, Facebook - If
                  that is what you want then this tool is a perfect place to go
                  because it provides more than that!Cool Fancy Text Generator
                  is a copy and paste font generator and font changer that
                  creates Twitter, Facebook, Instagram fonts. It converts a
                  normal text to different free cool fonts styles, such as
                  tattoo fonts, calligraphy fonts, web script fonts, cursive
                  fonts, handwriting fonts, old English fonts, word fonts,
                  pretty fonts, font art... Facebook, Twitter, Instagram Fonts
                  or Fonts for Instagram, Twitter, Facebook - If that is what
                  you want then this tool is a perfect place to go because it
                  provides more than that!
                </div>
              </div>
              <div className="page-info__description-preparation">
                <div className="page-info__ingredients">
                  <div className="page-info__stat-title">Ingredients</div>
                  <div className="page-info__list">
                    <div className="page-info__info-ingredients">
                      <h3>Water</h3>
                      <p>25 liters</p>
                    </div>
                    <div className="page-info__info-ingredients">
                      <h3>Malt</h3>
                      <p>"Extra Pale" - 5.3 kilograms</p>
                    </div>
                    <div className="page-info__info-ingredients">
                      <h3>Hops</h3>
                      <p>
                        "Ahtanum" - 17.5 grams, and when start "Chinook" - 15
                        grams, add when start
                      </p>
                    </div>
                    <div className="page-info__info-ingredients">
                      <h3>Yeast</h3>
                      <p>Wyeast 1056 - American Ale""</p>
                    </div>
                  </div>
                </div>
                <div className="page-info__method">
                  <div className="page-info__stat-title">Method</div>
                  <div className="page-info__info-method">
                    <h3>Mash</h3>
                    <p>75 minutes at 65 C</p>
                    <p>10 minutes at 72 C</p>=
                  </div>
                  <div className="page-info__info-method">
                    <h3>Fermentation</h3>
                    <p>Perform at 19 C</p>
                  </div>
                  <div className="page-info__info-method">
                    <h3>Twist</h3>
                    <p>Rasppberries in the boil, rhubard at maturation.</p>
                  </div>
                  <div className="page-info__info-method">
                    <h3>Yeast</h3>
                    <p>Wyeast 1056 - American Ale""</p>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>
      </div>
    );
  }
}

export default PageInfo;

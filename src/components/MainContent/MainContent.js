import React from "react";
import ContentBox from "../ContentBox/ContentBox";
import FormSearch from "../FormSearch/FormSearch";
import Slider from "../Slider/Slider";

class MainContent extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="main">
        <FormSearch
          change={this.props.change}
          textOfSearch={this.props.textOfSearch}
        />
        <div className="main__filter">
          <Slider
            max={10}
            step={0.1}
            name={0}
            value={this.props.valueAlcohol}
            onChangeValueOfSlider={this.props.onChangeValueOfSlider}
            title={"Alcohol by value"}
          />
          <Slider
            name={1}
            step={1}
            max={200}
            title={"International Bitterness Units"}
            value={this.props.valueIBU}
            onChangeValueOfSlider={this.props.onChangeValueOfSlider}
          />
          <Slider
            name={2}
            max={10}
            step={1}
            title={"Color by EBC"}
            value={this.props.valueEBC}
            onChangeValueOfSlider={this.props.onChangeValueOfSlider}
          />
        </div>
        <ContentBox massOfBeers={this.props.massOfBeers} />
      </div>
    );
  }
}

export default MainContent;

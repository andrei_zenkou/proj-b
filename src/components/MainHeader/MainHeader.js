import React from "react";
import "./MainHeader.scss";
import HeaderButton from "../HeaderButton/HeaderButton";
import HeaderLogo from "../HeaderLogo/HeaderLogo";
import HeaderSlide from "../HeaderSlide/HeaderSlide";

class MainHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stateOfButton: false
    };
  }
  changeStateOfButton() {
    if (!this.state.stateOfButton) {
      this.setState({
        stateOfButton: true
      });
    } else {
      this.setState({
        stateOfButton: false
      });
    }
  }
  render() {
    return (
      <div className="header header_button">
        <HeaderSlide
          state={this.state.stateOfButton}
          clickButton={this.changeStateOfButton.bind(this)}
        />

        <div className="header__block">
          <HeaderButton
            clickButton={this.changeStateOfButton.bind(this)}
            state={this.state.stateOfButton}
          />
          <HeaderLogo />
        </div>
      </div>
    );
  }
}

export default MainHeader;

import React from "react";
import MainContent from "../MainContent/MainContent";
import MainHeader from "../MainHeader/MainHeader";

class Search extends React.Component {
  constructor() {
    super();

    this.state = {
      massOfBeers: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
      textOfSearch: "",
      valueAlcohol: 0,
      valueIBU: 0,
      valueEBC: 0
    };
  }
  changeFormSearch(e) {
    this.setState({
      textOfSearch: e.target.value
    });
    console.log(this.state.textOfSearch);
  }

  onChangeValueOfSlider(value, e) {
    switch (e.attributes.name.nodeValue) {
      case "0":
        this.setState({
          valueAlcohol: value
        });
        break;
      case "1":
        this.setState({
          valueIBU: value
        });
        break;
      case "2":
        this.setState({
          valueEBC: value
        });
        break;
    }
  }
  render() {
    return (
      <div>
        <header>
          <MainHeader />
        </header>
        <main>
          <MainContent
            valueAlcohol={this.state.valueAlcohol}
            valueIBU={this.state.valueIBU}
            valueEBC={this.state.valueEBC}
            onChangeValueOfSlider={this.onChangeValueOfSlider.bind(this)}
            massOfBeers={this.state.massOfBeers}
            change={this.changeFormSearch.bind(this)}
            textOfSearch={this.state.textOfSearch}
          />
        </main>
      </div>
    );
  }
}
export default Search;

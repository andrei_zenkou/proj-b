import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import "./theme.scss";
import PageInfo from "./components/PageInfo/PageInfo";
import Search from "./components/Search/Search";

class App extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route path="/" exact component={Search} />
            <Route path="/page" component={PageInfo} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
